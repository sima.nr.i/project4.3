package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AwardCondition.class)
public abstract class AwardCondition_ {

	public static volatile SingularAttribute<AwardCondition, String> maxTimeForInstruement;
	public static volatile SingularAttribute<AwardCondition, String> maxAmountForInstruement;
	public static volatile SingularAttribute<AwardCondition, String> minAmountForInstruement;
	public static volatile SingularAttribute<AwardCondition, String> name;
	public static volatile SingularAttribute<AwardCondition, String> minTimeForInstruement;
	public static volatile SingularAttribute<AwardCondition, Integer> id;

}


package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FacilityAccount.class)
public abstract class FacilityAcount_ {

	public static volatile SingularAttribute<FacilityAccount, String> facilityAccountType;
	public static volatile SingularAttribute<FacilityAccount, Integer> id;
	public static volatile SingularAttribute<FacilityAccount, String> customerNumber;

}


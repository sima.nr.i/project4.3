package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Facilitytype.class)
public abstract class Facilities_ {

	public static volatile SingularAttribute<Facilitytype, String> interestRate;
	public static volatile SingularAttribute<Facilitytype, String> maxTimeForInstruement;
	public static volatile SingularAttribute<Facilitytype, String> nameOfFacility;
	public static volatile SingularAttribute<Facilitytype, String> maxAmountForInstruement;
	public static volatile SingularAttribute<Facilitytype, String> minAmountForInstruement;
	public static volatile SingularAttribute<Facilitytype, String> minTimeForInstruement;
	public static volatile SingularAttribute<Facilitytype, Integer> id;
	public static volatile SingularAttribute<Facilitytype, String> nameOfCondition;

}


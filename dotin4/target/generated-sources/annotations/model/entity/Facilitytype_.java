package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Facilitytype.class)
public abstract class Facilitytype_ {

	public static volatile SingularAttribute<Facilitytype, Float> interestRate;
	public static volatile SingularAttribute<Facilitytype, Long> maxTimeForInstruement;
	public static volatile SingularAttribute<Facilitytype, Long> maxAmountForInstruement;
	public static volatile SingularAttribute<Facilitytype, Long> minAmountForInstruement;
	public static volatile SingularAttribute<Facilitytype, String> name;
	public static volatile SingularAttribute<Facilitytype, Long> minTimeForInstruement;
	public static volatile SingularAttribute<Facilitytype, Integer> id;
	public static volatile SingularAttribute<Facilitytype, String> nameOfCondition;

}


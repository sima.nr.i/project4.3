package model.entity;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LegalCustomer.class)
public abstract class LegalCustomer_ {

	public static volatile SingularAttribute<LegalCustomer, String> companyName;
	public static volatile SingularAttribute<LegalCustomer, Integer> id;
	public static volatile SingularAttribute<LegalCustomer, Long> economicId;
	public static volatile SingularAttribute<LegalCustomer, Long> customerNumber;
	public static volatile SingularAttribute<LegalCustomer, Timestamp> dateOfRegistering;

}


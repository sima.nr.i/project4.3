<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 9/1/2020
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        {box-sizing: border-box}
        .container {
            position: relative;
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px 0 30px 0;
        }

        input,
        .btn {
            width: 100%;
            padding: 12px;
            border: none;
            border-radius: 4px;
            margin: 5px 0;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
            text-decoration: none; /* remove underline from anchors */
        }

        input:hover,
        .btn:hover {
            opacity: 1;
        }
        .fb {

            /*float: none;*/
            /*position: absolute;*/
            /*top: 50%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, -50%);*/
            text-align: center;
            background-color: #3B5998;
            color: white;
        }

        .twitter {

            /*float: none;*/
            /*position: absolute;*/
            /*top: 50%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, -50%);*/

            text-align: center;
            background-color: #55ACEE;
            color: white;
        }

        /* style the submit button */
        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        /* Two-column layout */
        .col {
            float: left;
            width: 50%;
            margin: auto;
            padding: 0 50px;
            margin-top: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* vertical line */
        .vl {
            position: absolute;
            left: 50%;
            transform: translate(-50%);
            border: 2px solid #ddd;
            height: 175px;
        }

        /* text inside the vertical line */
        .inner {
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%);
            background-color: #f1f1f1;
            border: 1px solid #ccc;
            border-radius: 50%;
            padding: 8px 10px;
        }

        /* hide some text on medium and large screens */
        .hide-md-lg {
            display: none;
        }

        /* bottom container */
        .bottom-container {
            text-align: center;
            background-color: #666;
            border-radius: 0px 0px 4px 4px;
        }

        /* Responsive layout - when the screen is less than 650px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 650px) {
            .col {
                width: 100%;
                margin-top: 0;
            }
            /* hide the vertical line */
            .vl {
                display: none;
            }
            /* show the hidden text on small screens */
            .hide-md-lg {
                display: block;
                text-align: center;
            }
        }



        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }



        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        input[type=number], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <title>Title</title>
    <script>
        function alphaOnly(event) {
            var key = event.keyCode;
            return ((key >= 65 && key <= 90) || key == 8);
        };
    </script>
</head>
<body>
<form action="/facilityController.do">
    <input type="hidden" name="action" value="save">
    <input readonly type="text" name="nameOfFacility" value="${sessionScope.nameOfFacility} " placeholder="<%out.print(request.getAttribute("nameOfFacility"));%>">
    <input readonly type="text" name="interestRate" value="${sessionScope.interestRate}" placeholder="<%out.print(request.getAttribute("interestRate"));%>">
    <input type="text" name="name" id="name" placeholder="name" onkeydown="return alphaOnly(event)">
    <input type="number" name="minTimeForInstruement" id="minTimeForInstruement" placeholder="minTimeForInstruement">
    <input type="number" name="maxTimeForInstruement" id="maxTimeForInstruement" placeholder="maxTimeForInstruement">
    <input type="number" name="minAmountForInstruement" id="minAmountForInstruement" placeholder="minAmountForInstruement">
    <input type="number" name="maxAmountForInstruement" id="maxAmountForInstruement" placeholder="maxAmountForInstruement">
    <input type="submit" value="ثبت شروط اعطا">
</form>
<table border="0" style="width: 100%" id="customers">
    <tr>
        <th>NAMEOFFACILITY</th>
        <th>INTERESTRATE</th>
        <th>NAME</th>
        <th>MINTIMEFORINSTRUEMENT</th>
        <th>MAXTIMEFORINSTRUEMENT</th>
        <th>MINAMOUNTFORINSTRUEMENT</th>
        <th>MAXTIMEFORINSTRUEMENT</th>
    </tr>
    <c:forEach items="${requestScope.listOfAwardCondition}" var="listOfAwardCondition">
        <tr>
            <td><c:out value="${listOfAwardCondition.nameOfFacility}"/></td>
            <td><c:out value="${listOfAwardCondition.interestRate}"/></td>
            <td><c:out value="${listOfAwardCondition.nameOfCondition}"/></td>
            <td><c:out value="${listOfAwardCondition.minTimeForInstruement}"/></td>
            <td><c:out value="${listOfAwardCondition.maxTimeForInstruement}"/></td>
            <td><c:out value="${listOfAwardCondition.minAmountForInstruement}"/></td>
            <td><c:out value="${listOfAwardCondition.maxAmountForInstruement}"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/3/2020
  Time: 1:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <title>Title</title>
</head>
    <script>
    function alphaOnly(event) {
        var key = event.keyCode;
        return ((key >= 65 && key <= 90) || key == 8);
    };
</script>

<body>

مشتری حقیقی
<form action="/realCustomerController.do">
    <input type="hidden" name="action" value="update">
    <input type="text" name="id" id="id"  value="${requestScope.myId}" placeholder="<%out.print(request.getParameter("id"));%>">
    <input type="text" name="name" placeholder="<%out.print(request.getAttribute("myName"));%>" onkeydown="return alphaOnly(event)">
    <input type="text" name="lastName" placeholder="<%out.print(request.getAttribute("myLastName"));%>" onkeydown="return alphaOnly(event)">
    <input type="text" name="identityNumber" placeholder="<%out.print(request.getAttribute("myIdentityNumber"));%>" onkeydown="return alphaOnly(event)">
    <input type="text" name="nameOfFather" placeholder="<%out.print(request.getAttribute("myNameOfFather"));%>" onkeydown="return alphaOnly(event)">
    <input type="date" name="dateOfBirth" placeholder="<%out.print(request.getAttribute("myDateOfBirth"));%>" onkeydown="return alphaOnly(event)">
    <input type="submit" value="ویرایش">
</form>
</br>
مشتری حقوقی
<form action="/legalCustomerController.do">
    <input type="hidden" name="action" value="update">
    <input type="text" name="companyId" placeholder="<%out.print(request.getParameter("id2"));%>" value="${requestScope.myId1}">
    <input type="text" name="companyName" placeholder="<%out.print(request.getAttribute("companyName"));%>" onkeydown="return alphaOnly(event)">
    <input type="text" name="economicId" placeholder="<%out.print(request.getAttribute("economicId"));%>" onkeydown="return alphaOnly(event)">
    <input type="date"  name="dateOfRegistering" placeholder="<%out.print(request.getAttribute("dateOfRegistering"));%>" onkeydown="return alphaOnly(event)">
    <input type="submit" value="ویرایش">
</form>
</body>
</html>

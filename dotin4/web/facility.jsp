<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 9/1/2020
  Time: 11:09 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <title>Title</title>
        <script>
        function alphaOnly(event) {
            var key = event.keyCode;
            return ((key >= 65 && key <= 90) || key == 8);
        };
    </script>

</head>
<body>

<form action="/facilityController.do" >
    <input type="hidden" name="action" value="beforeSaveFacility">
    <input type="text" name="nameOfFacility" id="nameOfFacility" placeholder="nameOfFacility" onkeydown="return alphaOnly(event)">
    <input type="number" name="interestRate" id="interestRate" placeholder="interestRate" step="0.01">
    <input type="submit" value="تعریف شود">
</form>
</body>
</html>

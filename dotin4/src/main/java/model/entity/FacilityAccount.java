package model.entity;

import javax.persistence.*;
import java.io.Serializable;
/**
 * Created by Hi on 9/12/2020.
 */
@Entity(name="facilityacount")
@Table(name="FACILITYACOUNT")
public class FacilityAccount implements Serializable{
    @Id
    @Column(columnDefinition = "NUMBER",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(nullable = false)
    private long customerNumber;
    @Column(columnDefinition = "VARCHAR(256)",nullable = false)
    private String facilityAccountType;
    public FacilityAccount(){}
    public FacilityAccount(long customerNumber, String facilityAccountType) {
        this.customerNumber = customerNumber;
        this.facilityAccountType = facilityAccountType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(Long customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getFacilityAccountType() {
        return facilityAccountType;
    }

    public void setFacilityAccountType(String facilityAccountType) {
        this.facilityAccountType = facilityAccountType;
    }
}

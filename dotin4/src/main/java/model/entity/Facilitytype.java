package model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Hi on 9/1/2020.
 */
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
@Entity(name ="facilitytype")
@Table(name = "FACILITYTYPE")

@NamedNativeQueries(
        {@NamedNativeQuery(name = "findAllNoReapeat", query = "select DISTINCT name,interestrate from facilitytype"),
        @NamedNativeQuery(name = "findMinMax",query = " select maxamountforinstruement,minamountforinstruement," +
                "maxtimeforinstruement,mintimeforinstruement from facilitytype where name=:name")
        }
)

public class Facilitytype implements Serializable {
    @Id
    @Column(columnDefinition = "NUMBER",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(columnDefinition = "VARCHAR(256)",nullable = false)
    private String name;
    @Column(nullable = false)
    private Float interestRate;
    @Column(columnDefinition = "VARCHAR(256)",nullable = false)
    private String nameOfCondition;
    @Column(nullable = false)
    private long minTimeForInstruement;
    @Column(nullable = false)
    private long maxTimeForInstruement;
    @Column(nullable = false)
    private long minAmountForInstruement;
    @Column(nullable = false)
    private long maxAmountForInstruement;

    public String getNameOfCondition() {
        return nameOfCondition;
    }

    public Facilitytype(String nameOfFacility, float interestRate, String nameOfCondition, long minTimeForInstruement, long maxTimeForInstruement, long minAmountForInstruement, long maxAmountForInstruement) {
        this.name = nameOfFacility;
        this.interestRate = interestRate;
        this.nameOfCondition = nameOfCondition;
        this.minTimeForInstruement = minTimeForInstruement;
        this.maxTimeForInstruement = maxTimeForInstruement;
        this.minAmountForInstruement = minAmountForInstruement;
        this.maxAmountForInstruement = maxAmountForInstruement;
    }

    public Facilitytype(String nameOfFacility, float interestRate) {
        this.name = nameOfFacility;
        this.interestRate = interestRate;
    }

    public Facilitytype(String nameOfCondition, long minTimeForInstruement, long maxTimeForInstruement, long minAmountForInstruement, long maxAmountForInstruement) {
        this.nameOfCondition = nameOfCondition;
        this.minTimeForInstruement = minTimeForInstruement;
        this.maxTimeForInstruement = maxTimeForInstruement;
        this.minAmountForInstruement = minAmountForInstruement;
        this.maxAmountForInstruement = maxAmountForInstruement;
    }

    public Facilitytype(){}

    public void setNameOfCondition(String nameOfCondition) {
        this.nameOfCondition = nameOfCondition;
    }

    public long getMinTimeForInstruement() {
        return minTimeForInstruement;
    }

    public void setMinTimeForInstruement(long minTimeForInstruement) {
        this.minTimeForInstruement = minTimeForInstruement;
    }

    public long getMaxTimeForInstruement() {
        return maxTimeForInstruement;
    }

    public void setMaxTimeForInstruement(long maxTimeForInstruement) {
        this.maxTimeForInstruement = maxTimeForInstruement;
    }

    public long getMinAmountForInstruement() {
        return minAmountForInstruement;
    }

    public void setMinAmountForInstruement(long minAmountForInstruement) {
        this.minAmountForInstruement = minAmountForInstruement;
    }

    public long getMaxAmountForInstruement() {
        return maxAmountForInstruement;
    }

    public void setMaxAmountForInstruement(long maxAmountForInstruement) {
        this.maxAmountForInstruement = maxAmountForInstruement;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameOfFacility() {
        return name;
    }

    public void setNameOfFacility(String nameOfFacility) {
        this.name = nameOfFacility;
    }

    public Float getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(float interestRate) {
        this.interestRate = interestRate;
    }
}

package model.entity;

import com.sun.istack.internal.NotNull;
import common.MyGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Hi on 8/1/2020.
 */
@Entity(name = "realcustomer")
@Table(name = "REALCUSTOMER")
public class RealCustomer implements Serializable {
    @Id
    @Column(columnDefinition = "NUMBER",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    @Column(unique = true,nullable = false)
    @GeneratedValue(generator = MyGenerator.generatorName)
    @GenericGenerator(name = MyGenerator.generatorName, strategy = "a.b.c.MyGenerator")
    private Long customerNumber;
    @Column(columnDefinition = "VARCHAR(256)",nullable = false)
    private String name;
    @Column(columnDefinition = "VARCHAR(256)",nullable = false)
    private String lastName;
    @Column(columnDefinition = "VARCHAR(256)",unique = true,nullable = false)
    @NotNull
    private Long identityNumber;
    @Column(columnDefinition = "VARCHAR(256)",nullable = false)
    private String nameOfFather;
    @Column(name = "dateOfBirth",nullable =false)
    private java.sql.Timestamp dateOfBirth;
    public RealCustomer(int id, long customerNumber, String name, String lastName, long identityNumber, String nameOfFather, Timestamp dateOfBirth) {
        this.id = id;
        this.customerNumber = customerNumber;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }
    //==================================================================================================================================
    public RealCustomer(String name, String lastName, long identityNumber, String nameOfFather, Timestamp dateOfBirth) {
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    public RealCustomer(int id, String name, String lastName, long identityNumber, String nameOfFather, Timestamp dateOfBirth) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    public RealCustomer(int id, String name, String lastName, long identityNumber) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
    }

    public RealCustomer() {
    }

    //=====================================================================================================================================
    public long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(long customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(long identityNumber) {
        this.identityNumber = identityNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfFather() {
        return nameOfFather;
    }

    public void setNameOfFather(String nameOfFather) {
        this.nameOfFather = nameOfFather;
    }

    public Timestamp getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Timestamp dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}

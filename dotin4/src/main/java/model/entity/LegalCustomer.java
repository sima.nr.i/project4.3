package model.entity;

import com.sun.istack.internal.NotNull;
import common.MyGenerator;
import common.TimeStamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Table;

import javax.persistence.Entity;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Hi on 8/1/2020.
 */
@Entity(name ="legalcustomer")
@javax.persistence.Table(name="LEGALCUSTOMER")

public class LegalCustomer implements Serializable {
    @Id
    @Column(columnDefinition = "NUMBER",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    @Column(unique = true,nullable = false)
    private Long customerNumber;
    @Column(columnDefinition = "VARCHAR(256)",nullable = false)
    private String companyName;
    @Column(nullable = false)
    private Long economicId;
    @Column(name = "dateOfRegistering",nullable = false)
    private java.sql.Timestamp dateOfRegistering;
    public LegalCustomer(Integer id, Long customernumber, String companyname, Long economicid, Timestamp dateofregistering) {
        this.id = id;
        this.customerNumber = customernumber;
        this.companyName = companyname;
        this.economicId = economicid;
        this.dateOfRegistering = dateofregistering;
    }

    public LegalCustomer(Long customerNumber, String companyName, Long economicId, Timestamp dateOfRegistering) {
        this.customerNumber = customerNumber;
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    //================================================================================================================================
    public LegalCustomer(String companyName, Long economicId, Timestamp dateOfRegistering) {
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    public LegalCustomer(Integer id, String companyName, Long economicId, Timestamp dateOfRegistering) {
        this.id = id;
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    public LegalCustomer(Integer id, String companyName, Long economicId) {
        this.id = id;
        this.companyName = companyName;
        this.economicId = economicId;
    }

    public LegalCustomer() {
    }

    public Long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(long customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getEconomicId() {
        return economicId;
    }

    public void setEconomicId(long economicId) {
        this.economicId = economicId;
    }

    public Timestamp getDateOfRegistering() {
        return dateOfRegistering;
    }

    public void setDateOfRegistering(Timestamp dateOfRegistering) {
        this.dateOfRegistering = dateOfRegistering;
    }
}

package controller;

import common.service.RealCustomerService;
import model.entity.RealCustomer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/22/2020.
 */
@WebServlet("/beforeUpdate.do")
public class BeforeUpdate extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(FacilityAccountController.class);
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setAttribute("myId",req.getParameter("id"));
            RealCustomerService realCustomerService=new RealCustomerService();
            RealCustomer realCustomer=new RealCustomer();
            realCustomer=realCustomerService.findOneById(Integer.parseInt(req.getParameter("id")));
            req.setAttribute("myName",realCustomer.getName());
            req.setAttribute("myLastName",realCustomer.getLastName());
            req.setAttribute("myIdentityNumber",realCustomer.getIdentityNumber());
            req.setAttribute("myNameOfFather",realCustomer.getNameOfFather());
            req.setAttribute("myDateOfBirth",realCustomer.getDateOfBirth());
            req.getRequestDispatcher("/edit.jsp").forward(req, resp);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }


}

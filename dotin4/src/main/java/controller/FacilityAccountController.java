package controller;

import common.service.FacilityTypeService;
import common.service.FacilityAccountService;
import common.service.RealCustomerService;
import model.entity.FacilityAccount;
import model.entity.Facilitytype;
import model.entity.RealCustomer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Hi on 9/13/2020.
 */
@WebServlet("/facilityAccountController.do")
public class FacilityAccountController extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(FacilityAccountController.class);
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action;
            action = req.getParameter("action");
        switch (action) {
            case "facilityaccount":
                facilityAccount(req,res);
                break;
            case "findNameFamily":
                findNameFamily(req,res);
                break;
            case "checkCondition":
                checkCondition(req,res);
                break;
            case "facilitySelectFile":
                facilitySelectFile(req,res);
                break;
            case "facilityAccountSave":
                    facilityAccountSave(req,res);
                            break;
        }
    }
    public void facilityAccount(HttpServletRequest req,HttpServletResponse resp) throws IOException {
         String customernumber;
        try {
            customernumber=req.getParameter("customernumber");
            req.setAttribute("customernumberforfacility",req.getParameter("customernumber"));
            findNameFamily(req,resp);
        } catch (Exception e) {
            logger.info(e.getMessage());
            resp.sendRedirect("error.jsp");
        }
    }
    public void findNameFamily(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            RealCustomerService realCustomerService = new RealCustomerService();
            if (req.getParameter("customerNumber01")==null||req.getParameter("customerNumber01")=="")
            {
                resp.sendRedirect("error.jsp");
            }
            else {
                req.getServletContext().setAttribute("customerNumberForFacilityAccount", req.getParameter("customerNumber01"));
                List<RealCustomer> realCustomers = realCustomerService.findNameFamily(req.getParameter("customerNumber01"));
                if (realCustomers.size()==0)
                {
                    req.setAttribute("checkFindNameFamily","false");
                    req.getRequestDispatcher("error.jsp").forward(req,resp);
                    logger.info("CUSTOMERNUMBR IS WRONG,NOT SHOW NAME AND FAMILY FOR THIS CUSTOMERNUMBER");
                }
                else {
                    req.setAttribute("list03", realCustomerService.findNameFamily(req.getParameter("customerNumber01")));
                    System.out.println();
                    req.getRequestDispatcher("/file.jsp").forward(req, resp);
                }
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        finally {
            logger.info("FINDNAMEFAMILY METHOD IS START");
        }
    }
    public void checkCondition(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            FacilityTypeService facilityTypeService = new FacilityTypeService();
            System.out.println("nameoffacility"+req.getParameter("nameOfFacility"));
            List<Object> facilities= facilityTypeService.findMinMax(req.getParameter("nameOfFacility"));
            if (facilities.size()>0)
            {
                System.out.println("notnull");
            }
            else System.out.println("null");
            String amount=req.getParameter("amount");
            String time=req.getParameter("time");
            if (amount==null||time==null||amount==""||time=="")
            {
                resp.sendRedirect("error.jsp");
            }
            else {
            List<Facilitytype> facilitytype1 =new ArrayList<Facilitytype>();
            Iterator iterator=facilities.iterator();
            boolean check=false;
            while (iterator.hasNext())
            {
                Object[] objectList= (Object[]) iterator.next();
                BigDecimal maxAmountBigDecimal= (BigDecimal) objectList[0];
                String maxAmountString=maxAmountBigDecimal.toString();
                BigDecimal minAmountBigDecimal=(BigDecimal) objectList[1];
                String minAmountString=minAmountBigDecimal.toString();
                BigDecimal maxTimeBigDecimal=(BigDecimal) objectList[2];
                String maxTimeString=maxTimeBigDecimal.toString();
                BigDecimal minTimeBigDecimal=(BigDecimal) objectList[3];
                String minTimeString=minTimeBigDecimal.toString();
                if (Integer.parseInt(amount)<Integer.parseInt(maxAmountString)
                        && Integer.parseInt(amount)> Integer.parseInt(minAmountString)
                        && Integer.parseInt(time)<Integer.parseInt(maxTimeString)
                        && Integer.parseInt(time)>Integer.parseInt(minTimeString))
                {
                    check=true;
                    req.getServletContext().setAttribute("nameOfFacilityInRegistering",req.getParameter("nameOfFacility"));
                    req.setAttribute("checkCondition","true");
                    req.getRequestDispatcher("file.jsp").forward(req,resp);
//                    String customerNumberString=(req.getServletContext().getAttribute("customerNumberForFacilityAccount")).toString();
//                    Long customerNumberLong=Long.parseLong(customerNumberString);
//                    FacilityAccount facilityAccount=new FacilityAccount(customerNumberLong,(String) req.getParameter("nameOfFacility"));
//                    FacilityAccountService facilityAccountService=new FacilityAccountService();
//                    facilityAccountService.save(facilityAccount);
                    logger.info("1 file is saved");
                    break;
                }

            }

            if (check)
                resp.sendRedirect("/check.jsp");
            else {
                logger.info("THERE IS NO MATCHING BETWEEN CONDITION OF CUSTOMER AND CONDITION OF FACILITYFILE,SO NO FILE MAKE");
                req.setAttribute("check","conditionNotTrue");
                req.getRequestDispatcher("error.jsp").forward(req,resp);

            }}

        } catch (Exception e) {
            logger.info(e.getMessage());

        }
        finally {
            logger.info("METHOD CHECK CONDITION IS START");
        }
    }
    public void facilityAccountSave(HttpServletRequest req,HttpServletResponse resp)
    {
        String customerNumberString=(req.getServletContext().getAttribute("customerNumberForFacilityAccount")).toString();
        Long customerNumberLong=Long.parseLong(customerNumberString);
        FacilityAccount facilityAccount=new FacilityAccount(customerNumberLong,(String)req.getServletContext().getAttribute("nameOfFacilityInRegistering"));
        FacilityAccountService facilityAccountService=new FacilityAccountService();
        facilityAccountService.save(facilityAccount);
        req.setAttribute("messageForFacilityAccountSave","ok");
        try {
            req.getRequestDispatcher("file.jsp").forward(req,resp);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void facilitySelectFile(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            FacilityTypeService facilityTypeService = new FacilityTypeService();
            List<Object> facilities= facilityTypeService.findAllNoReapeat();
            List<Facilitytype> facilitytype1 =new ArrayList<Facilitytype>();
            Iterator iterator=facilities.iterator();
            while (iterator.hasNext())
            {
                Object[] objectList= (Object[]) iterator.next();
                Facilitytype facilitytype2 =new Facilitytype();
                facilitytype2.setInterestRate((Float) objectList[1]);
                facilitytype2.setNameOfFacility((String) objectList[0]);
                facilitytype1.add(facilitytype2);
            }
            req.getSession().setAttribute("list05", facilitytype1);
            req.getRequestDispatcher("/file.jsp").forward(req, resp);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }


}

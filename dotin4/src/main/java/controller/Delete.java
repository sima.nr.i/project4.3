package controller;

import common.service.RealCustomerService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/1/2020.
 */
@WebServlet("/delete.do")
public class Delete extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(FacilityAccountController.class);
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            RealCustomerService realCustomerService = new RealCustomerService();
            realCustomerService.delete(Integer.parseInt(req.getParameter("id")));
            req.getSession().setAttribute("action","findAll");
            req.getRequestDispatcher("/realCustomerController.do").forward(req,resp);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }


}

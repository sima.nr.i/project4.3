package controller;

import common.service.LegalCustomerService;
import model.entity.LegalCustomer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/22/2020.
 */
@WebServlet("/beforeLegalUpdate.do")
public class BeforelegalUpdate extends HttpServlet{
    private static final Logger logger = LogManager.getLogger(FacilityAccountController.class);
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setAttribute("myId1",req.getParameter("id3"));
            LegalCustomerService legalCustomerService=new LegalCustomerService();
            LegalCustomer legalCustomer=new LegalCustomer();
            legalCustomer=legalCustomerService.findOneById(Integer.parseInt(req.getParameter("id3")));
            req.setAttribute("companyName",legalCustomer.getCompanyName());
            req.setAttribute("economicId",legalCustomer.getEconomicId());
            req.setAttribute("dateOfRegistering",legalCustomer.getDateOfRegistering());
            req.getRequestDispatcher("/edit.jsp").forward(req, resp);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }


}

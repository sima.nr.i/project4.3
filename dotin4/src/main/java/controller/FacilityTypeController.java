package controller;

import common.service.FacilityTypeService;
import model.entity.Facilitytype;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 9/13/2020.
 */
@WebServlet("/facilityController.do")
public class FacilityTypeController extends HttpServlet{
    private static final Logger logger = LogManager.getLogger(FacilityTypeController.class);
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action;
        if (req.getParameter("action")==null)
            action= (String) req.getSession().getAttribute("action");
        else
            action = req.getParameter("action");
        switch (action) {
            case "save":
                save(req,res);
                break;
            case "findAll":
                findAll(req,res);
                break;
            case "beforeSaveFacility":
                beforeSaveFacility(req,res);
                break;
        }
    }
    public void beforeSaveFacility(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        try {
            Double interestRate;
            if (req.getParameter("nameOfFacility")==null ||req.getParameter("interestRate")==null||
                    req.getParameter("nameOfFacility")=="" ||req.getParameter("interestRate")=="")
            {
                resp.sendRedirect("error.jsp");
            }
            else {
                req.getSession().setAttribute("nameOfFacility", req.getParameter("nameOfFacility"));
                req.getSession().setAttribute("interestRate", req.getParameter("interestRate"));
                req.getRequestDispatcher("/awardCondition.jsp").forward(req, resp);
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            resp.sendRedirect("error.jsp");
        }
    }
    public void save(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        if (req.getParameter("nameOfFacility")==""||req.getParameter("interestRate")==""||
                req.getParameter("name")==""||req.getParameter("mintimeforinstruement")==""||
                req.getParameter("maxtimeforinstruement")==""||
        req.getParameter("minamountforinstruement")==""||
                req.getParameter("maxamountforinstruement")=="")
            resp.sendRedirect("error.jsp");
        else {
            Facilitytype facilitytype = new Facilitytype(req.getParameter("nameOfFacility"), Float.parseFloat(req.getParameter("interestRate")), req.getParameter("name")
                    , Long.parseLong(req.getParameter("minTimeForInstruement")),Long.parseLong(req.getParameter("maxTimeForInstruement")), Long.parseLong(req.getParameter("minAmountForInstruement")),
                    Long.parseLong(req.getParameter("maxAmountForInstruement")));
            try {
                FacilityTypeService facilityTypeService = new FacilityTypeService();
                facilityTypeService.save(facilitytype);
                logger.info("1 FACILITY IS SAVED");
                findAll(req,resp);
            } catch (Exception e) {
                logger.info(e.getMessage());
            }
            finally {
                logger.info("SAVE METHOD FOR FACILITY TYPE IS START");
            }
        }
    }
    public void findAll(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        try {
            FacilityTypeService facilityTypeService = new FacilityTypeService();
            req.setAttribute("listOfAwardCondition", facilityTypeService.findAll());
            req.getRequestDispatcher("/awardCondition.jsp").forward(req,resp);
        } catch (Exception e) {
            logger.info(e.getMessage());
            resp.sendRedirect("error.jsp");
        }

    }




}

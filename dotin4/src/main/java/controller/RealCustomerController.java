package controller;

import common.TimeStamp;
import common.Value;
import common.service.RealCustomerService;
import model.entity.RealCustomer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Hi on 9/13/2020.
 */
@WebServlet("/realCustomerController.do")
public class RealCustomerController extends HttpServlet{
    private static final Logger logger = LogManager.getLogger(RealCustomerController.class);
    private RealCustomerService realCustomerService=new RealCustomerService();
    @Override
    public void service(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
        String action = null;
        if (req.getParameter("action")==null)
            action=(String) req.getSession().getAttribute("action");
        else
        action = req.getParameter("action");
        System.out.println("action:"+action);
        switch(action){
            case "save":
                    save(req,res);
                break;
            case "findAll":
                findAll(req,res);
                break;
            case "update":
                    update(req,res);
                break;
            case "delete":
                delete(req,res);
                break;
            case "search":
                search(req,res);
                break;

        }
}
public void save(HttpServletRequest req,HttpServletResponse resp) throws IOException{
        try {
            Timestamp dateOfBirth;
            if (req.getParameter("dateOfBirth") == "")
                dateOfBirth = null;
            else
            dateOfBirth=TimeStamp.convertStringToTimestamp(req.getParameter("dateOfBirth"));
            RealCustomer realCustomer = new RealCustomer(req.getParameter("name"), req.getParameter("lastName")
                    , Long.parseLong(req.getParameter("identityNumber")), req.getParameter("nameOfFather"), dateOfBirth);
                RealCustomerService realCustomerService = new RealCustomerService();
                realCustomerService.save(realCustomer);
            logger.info("1 REALCUSTOMER IS SAVED");
                req.setAttribute("id", realCustomer.getId());
                showCustomerNumber(req, resp);

        }catch (Exception e)
        {
            logger.info(e.getMessage());
            resp.sendRedirect("error.jsp");
        }
            finally {
                logger.info("METHOD SAVE START");
            }

}
public void search(HttpServletRequest req,HttpServletResponse resp)
{
    try {
        RealCustomer realCustomer;
        Long identityNumber;
        if (req.getParameter("identityNumber")=="")
        {
             identityNumber=Value.zero;
        }
        else
            identityNumber=Long.parseLong(req.getParameter("identityNumber"));
        if (req.getParameter("id")=="")
        {
            realCustomer =new RealCustomer(-1,req.getParameter("name"),
                    req.getParameter("lastName"),identityNumber);

        }
        else
            realCustomer =new RealCustomer(Integer.parseInt(req.getParameter("id")),req.getParameter("name"),
                    req.getParameter("lastName"),identityNumber);
        RealCustomerService realCustomerService = new RealCustomerService();
        req.setAttribute("list", realCustomerService.search(realCustomer));
        req.getRequestDispatcher("/managingPersoncustomer.jsp").forward(req, resp);
    } catch (Exception e) {
        logger.info(e.getMessage());
    }

}
public void findAll(HttpServletRequest request,HttpServletResponse response)
{
    try {
        RealCustomerService realCustomerService = new RealCustomerService();
        request.setAttribute("list", realCustomerService.findAll());
        request.getRequestDispatcher("/managingPersoncustomer.jsp").forward(request, response);
    } catch (Exception e) {
        logger.info(e.getMessage());
    }
}
public void update(HttpServletRequest req, HttpServletResponse resp){
        try {
            RealCustomerService realCustomerService = new RealCustomerService();
            Long identityNumber;
            Timestamp dateOfBirth;
            if (req.getParameter("identityNumber") == "" || req.getParameter("identityNumber") == null) {
                identityNumber = Value.zero;
            } else
                identityNumber = Long.parseLong(req.getParameter("identityNumber"));
            if (req.getParameter("dateOfBirth") == "")
                dateOfBirth = null;
            else
            dateOfBirth= TimeStamp.convertStringToTimestamp(req.getParameter("dateOfBirth"));
            RealCustomer realCustomer = new RealCustomer(Integer.parseInt(req.getParameter("id")), req.getParameter("name"), req.getParameter("lastName")
                    , identityNumber, req.getParameter("nameOfFather"), dateOfBirth);
            realCustomerService.update(realCustomer);
            findAll(req, resp);
        } catch (Exception e) {
        logger.info(e.getMessage());
    }
}
public void delete(HttpServletRequest req,HttpServletResponse resp)
{
    try {
        RealCustomerService realCustomerService = new RealCustomerService();
        realCustomerService.delete(Integer.parseInt(req.getParameter("id")));
        findAll(req,resp);
    } catch (Exception e) {
        logger.info(e.getMessage());
    }
}
public void showCustomerNumber(HttpServletRequest req,HttpServletResponse resp)
{
    try {
        RealCustomerService realCustomerService = new RealCustomerService();
        req.setAttribute("customerNumberForPerson",realCustomerService.showCustomerNumber((Integer) req.getAttribute("id")));
        req.getRequestDispatcher("/showingCustomernumberForPerson.jsp").forward(req,resp);
    } catch (Exception e) {
        logger.info(e.getMessage());
    }
}


}

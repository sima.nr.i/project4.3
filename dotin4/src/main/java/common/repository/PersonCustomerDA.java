package common.repository;

import model.entity.RealCustomer;

import java.sql.*;
import java.util.*;

/**
 * Created by Hi on 8/1/2020.
 */
public class PersonCustomerDA {
//    Random random = new Random();
//    private PreparedStatement preparedStatement;
//    private int countCustomers = 0;
//    public static List<Integer> customernumbers = new ArrayList<Integer>();
//    private Connection connection;
//    public PersonCustomerDA() throws Exception {
//        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
//        connection = DriverManager.getConnection("jdbc:mysql://localhost/dotin3?" + "user=sima&password=myjava123");
//    }
//    public void insert(RealCustomer realCustomer) throws Exception {
//        preparedStatement = connection.prepareStatement("insert into personcustomer" +
//                " (customernumber,name,lastname,identitynumber,nameoffather,dateofbirth)" +
//                " values (?,?,?,?,?,?)");
//        countCustomers++;
//        for (int i = 100; i < 1000; i++) {
//            customernumbers.add(new Integer(i));
//        }
//        Collections.shuffle(customernumbers);
//        preparedStatement.setInt(1, (customernumbers.get(countCustomers)));
//        preparedStatement.setString(2, realCustomer.getName());
//        System.out.println(realCustomer.getLastName());
//        preparedStatement.setString(3, realCustomer.getLastName());
//        preparedStatement.setString(4, realCustomer.getIdentityNumber());
//        preparedStatement.setString(5, realCustomer.getNameOfFather());
//        preparedStatement.setString(6, realCustomer.getDateOfBirth());
//        preparedStatement.executeUpdate();
//    }
//
//    public String selectCustomerNumber() throws Exception {
//        preparedStatement = connection.prepareStatement("SELECT customernumber FROM dotin3.personcustomer ORDER BY id DESC LIMIT 0, 1\n");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        String customerNumber = null;
//        while (resultSet.next()) {
//            customerNumber = resultSet.getString("customernumber");
//        }
//        return customerNumber;
//    }
//
//    public List<RealCustomer> select() throws Exception {
//        preparedStatement = connection.prepareStatement("select * from personcustomer ");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        List<RealCustomer> personList = new ArrayList<RealCustomer>();
//        while (resultSet.next()) {
//            RealCustomer person = new RealCustomer(
//                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
//                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
//                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
//            personList.add(person);
//        }
//        return personList;
//    }
//
//    public RealCustomer searchbyid(int id)throws Exception{
//        preparedStatement=connection.prepareStatement("SELECT * from dotin3.personcustomer WHERE id=?");
//        preparedStatement.setInt(1,id);
//        ResultSet resultSet=preparedStatement.executeQuery();
//        RealCustomer person =null;
//        while (resultSet.next()){
//             person = new RealCustomer(
//                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
//                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
//                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
//        }
//        return person;
//
//    }
//    public List<RealCustomer> search(RealCustomer realCustomer)throws Exception{
//        preparedStatement=connection.prepareStatement("select * FROM dotin3.personcustomer " +
//                "WHERE (1=? or id=?) AND (1=? or name=?)AND (1=? or lastname=?) AND (1=? or identitynumber=?) ");
//        if (realCustomer.getId()==-1) {
//            preparedStatement.setInt(1,1);
//
//        }
//        else {
//            preparedStatement.setInt(1,0);
//        }
//        preparedStatement.setInt(2, realCustomer.getId());
//        if (realCustomer.getName()==null|| realCustomer.getName()=="")
//        {
//            preparedStatement.setInt(3,1);
//        }
//        else preparedStatement.setInt(3,0);
//        preparedStatement.setString(4, realCustomer.getName());
//        if (realCustomer.getLastName()==null|| realCustomer.getLastName()=="")
//        {
//            preparedStatement.setInt(5,1);
//        }
//        else preparedStatement.setInt(5,0);
//        System.out.println("personcustomer.lastname"+ realCustomer.getLastName());
//        preparedStatement.setString(6, realCustomer.getLastName());
//        if (realCustomer.getIdentityNumber()==null|| realCustomer.getIdentityNumber()=="")
//        {
//            preparedStatement.setInt(7,1);
//        }
//        else preparedStatement.setInt(7,0);
//        preparedStatement.setString(8, realCustomer.getIdentityNumber());
//        ResultSet resultSet=preparedStatement.executeQuery();
//        List<RealCustomer> realCustomers =new ArrayList<RealCustomer>();
//        while (resultSet.next()) {
//            RealCustomer person = new RealCustomer(
//                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
//                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
//                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
//            realCustomers.add(person);
//        }
//        return realCustomers;
//    }
//
//    public void delete(int id) throws Exception {
//        preparedStatement = connection.prepareStatement("delete from personcustomer where id=?");
//        preparedStatement.setInt(1, id);
//        preparedStatement.executeUpdate();
//    }
//
//    public void update(RealCustomer person) throws Exception {
//         RealCustomer realCustomer =searchbyid(person.getId());
//        preparedStatement = connection.prepareStatement
//                ("update dotin3.personcustomer set dotin3.personcustomer.name=?,dotin3.personcustomer.lastname=?," +
//                        "dotin3.personcustomer.identitynumber=?,dotin3.personcustomer.nameoffather=?," +
//                        "dotin3.personcustomer.dateofbirth=? where dotin3.personcustomer.id=?");
//        if(person.getName()==null || person.getName()=="")
//            preparedStatement.setString(1, realCustomer.getName());
//        else
//        preparedStatement.setString(1, person.getName());
//        if (person.getLastName()==null || person.getLastName()=="")
//            preparedStatement.setString(2, realCustomer.getLastName());
//        else
//        preparedStatement.setString(2, person.getLastName());
//        if (person.getIdentityNumber()==null || person.getIdentityNumber()=="")
//            preparedStatement.setString(3, realCustomer.getIdentityNumber());
//        else
//        preparedStatement.setString(3, person.getIdentityNumber());
//        if (person.getNameOfFather()==null || person.getNameOfFather()=="")
//            preparedStatement.setString(4, realCustomer.getNameOfFather());
//        else
//        preparedStatement.setString(4, person.getNameOfFather());
//        if (person.getDateOfBirth()==null || person.getDateOfBirth()=="")
//            preparedStatement.setString(5, realCustomer.getDateOfBirth());
//        else
//        preparedStatement.setString(5, person.getDateOfBirth());
//        preparedStatement.setInt(6, person.getId());
//        preparedStatement.executeUpdate();
//    }
//
//    public void close() throws Exception {
//        preparedStatement.close();
//        connection.close();
//    }
}

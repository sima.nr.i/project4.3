package common.repository;

import model.entity.LegalCustomer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class CompanyCustomerDA {
//    private PreparedStatement preparedStatement;
//    private Connection connection;
//    private int countCustomers = 0;
//    private List<Integer> customernumbers = new ArrayList<Integer>();
//
//    public CompanyCustomerDA() throws Exception {
//        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
//        connection = DriverManager.getConnection("jdbc:mysql://localhost/dotin3?" + "user=sima&password=myjava123");
//
//    }
//
//    public void insert(LegalCustomer legalCustomer) throws Exception {
//        preparedStatement = connection.prepareStatement("insert into companycustomer" +
//                " (customernumber,companyname,economicid,dateofregistering)" +
//                " values (?,?,?,?)");
//        countCustomers++;
//        for (int i = 100; i < 1000; i++) {
//            customernumbers.add(new Integer(i));
//        }
//        Collections.shuffle(customernumbers);
//        preparedStatement.setString(1, String.valueOf(customernumbers.get(countCustomers)));
//        preparedStatement.setString(2, legalCustomer.getCompanyName());
//        preparedStatement.setString(3, legalCustomer.getEconomicId());
//        preparedStatement.setString(4, legalCustomer.getDateOfRegistering());
//        preparedStatement.executeUpdate();
//    }
//
//    public String selectCustomerNumber() throws Exception {
//        preparedStatement = connection.prepareStatement("SELECT customernumber FROM dotin3.companycustomer ORDER BY id DESC LIMIT 0, 1\n");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        String customerNumber = null;
//        while (resultSet.next()) {
//            customerNumber = resultSet.getString("customernumber");
//        }
//        return customerNumber;
//    }
//
//    public List<LegalCustomer> select() throws Exception {
//        preparedStatement = connection.prepareStatement("select * from companycustomer ");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        List<LegalCustomer> legalCustomers = new ArrayList<LegalCustomer>();
//        while (resultSet.next()) {
//            LegalCustomer legalCustomer = new LegalCustomer(
//                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
//                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
//            legalCustomers.add(legalCustomer);
//        }
//        return legalCustomers;
//    }
//
//    public List<LegalCustomer> search(LegalCustomer legalCustomer)throws Exception{
//        preparedStatement=connection.prepareStatement("select * FROM dotin3.companycustomer " +
//                "WHERE (1=? or id=?) AND (1=? or companyname=?)AND (1=? or economicid=?) ");
//        if (legalCustomer.getId()==-1)
//        {
//            preparedStatement.setInt(1,1);
//        }
//        else preparedStatement.setInt(1,0);
//        preparedStatement.setInt(2, legalCustomer.getId());
//        if (legalCustomer.getCompanyName()==null || legalCustomer.getCompanyName()=="")
//        {
//            preparedStatement.setInt(3,1);
//        }
//        else preparedStatement.setInt(3,0);
//        preparedStatement.setString(4, legalCustomer.getCompanyName());
//        if (legalCustomer.getEconomicId()==null || legalCustomer.getEconomicId()=="")
//        {
//            preparedStatement.setInt(5,1);
//        }
//        else preparedStatement.setInt(5,0);
//        preparedStatement.setString(6, legalCustomer.getEconomicId());
//        ResultSet resultSet=preparedStatement.executeQuery();
//        List<LegalCustomer> legalCustomers =new ArrayList<LegalCustomer>();
//        while (resultSet.next()) {
//            LegalCustomer customer = new LegalCustomer(
//                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
//                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
//
//            legalCustomers.add(customer);
//        }
//        return legalCustomers;
//    }
//    public LegalCustomer searchbyid(int id) throws Exception {
//        preparedStatement=connection.prepareStatement("select * from companycustomer WHERE id =?");
//        preparedStatement.setInt(1, id);
//        ResultSet resultSet = preparedStatement.executeQuery();
//        LegalCustomer legalCustomer =null;
//        while (resultSet.next()) {
//             legalCustomer = new LegalCustomer(
//                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
//                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
//        }
//        return legalCustomer;
//    }
//    public void delete(int id) throws Exception {
//        preparedStatement = connection.prepareStatement("delete from companycustomer where id=?");
//        preparedStatement.setInt(1, id);
//        preparedStatement.executeUpdate();
//    }
//
//    public void update(LegalCustomer legalCustomer) throws Exception {
//        LegalCustomer legalCustomer1 =searchbyid(legalCustomer.getId());
//        preparedStatement = connection.prepareStatement
//                ("update dotin3.companycustomer set dotin3.companycustomer.companyname=?,dotin3.companycustomer.economicid=?," +
//                        "dotin3.companycustomer.dateofregistering=? where dotin3.companycustomer.id=?");
//        if(legalCustomer.getCompanyName()==null || legalCustomer.getCompanyName()=="")
//            preparedStatement.setString(1, legalCustomer1.getCompanyName());
//        else
//            preparedStatement.setString(1, legalCustomer.getCompanyName());
//        if (legalCustomer.getEconomicId()==null || legalCustomer.getEconomicId()=="")
//            preparedStatement.setString(2, legalCustomer1.getEconomicId());
//        else
//            preparedStatement.setString(2, legalCustomer.getEconomicId());
//        if (legalCustomer.getDateOfRegistering()==null || legalCustomer.getDateOfRegistering()=="")
//            preparedStatement.setString(3, legalCustomer1.getDateOfRegistering());
//        else
//            preparedStatement.setString(3, legalCustomer.getDateOfRegistering());
//
//        preparedStatement.setInt(4, legalCustomer.getId());
//        preparedStatement.executeUpdate();
//    }
//
//    public void close() throws Exception {
//        preparedStatement.close();
//        connection.close();
//    }
}

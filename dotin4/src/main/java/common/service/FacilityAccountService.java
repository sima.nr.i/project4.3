package common.service;

import common.JPA;
import model.entity.FacilityAccount;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Created by Hi on 9/12/2020.
 */
public class FacilityAccountService  {

    public void save(FacilityAccount facilityAccount)
    {
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(facilityAccount);
        entityTransaction.commit();
        entityManager.close();
    }
}

package common.service;

import com.sun.org.apache.regexp.internal.RE;
import common.JPA;
import common.Value;
import model.entity.RealCustomer;
import common.repository.PersonCustomerDA;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.jpamodelgen.xml.jaxb.Entity;
import org.hibernate.jpamodelgen.xml.jaxb.Persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Hi on 8/1/2020.
 */
public class RealCustomerService {
    public int countCustomers= (int) Value.zero;
    public  List<Integer> customerNumbers = new ArrayList<Integer>();
    public  void save(RealCustomer realCustomer) throws Exception {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        personCustomerDA.insert(person);
//        personCustomerDA.close();
        countCustomers++;
        for (int i = Value.hundred; i < Value.thousand; i++) {
            customerNumbers.add(new Integer(i));
        }
        Collections.shuffle(customerNumbers);
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        realCustomer.setCustomerNumber(Long.parseLong(String.valueOf((customerNumbers.get(countCustomers)))));
        entityManager.persist(realCustomer);
        entityTransaction.commit();
        entityManager.close();
    }

    public  Long showCustomerNumber(int id)throws Exception
    {
//        PersonCustomerDA personCustomerDA=new PersonCustomerDA();
//        String customerNumber=personCustomerDA.selectCustomerNumber();
//        personCustomerDA.close();
//        return customerNumber;
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomer02=entityManager.find(RealCustomer.class,id);
        Long customerNumber=realCustomer02.getCustomerNumber();
        entityTransaction.commit();
        entityManager.close();
        return customerNumber;

    }
    public  List<RealCustomer> findAll()throws Exception
    {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        List<RealCustomer> personList = personCustomerDA.select();
//        personCustomerDA.close();
//        return personList;

        //FIND ALL BY JPQL
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createQuery("select allData from realcustomer allData");
        List<RealCustomer> realCustomers=query.getResultList();
        entityManager.close();
        return realCustomers;
    }

    public  List<RealCustomer> search(RealCustomer realCustomer)throws Exception
    {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        List<RealCustomer> personList = personCustomerDA.search(realCustomer);
//        personCustomerDA.close();
//        return personList;
        EntityManager entityManager=JPA.getManager();
        Query  query=entityManager.createQuery("select data from realcustomer data where" +
                "(1=:checkId or data.id=:id) AND (1=:checkName or data.name=:name)AND(1=:checkLastName or data.lastName=:lastName) AND(1=:checkIdentityNumber or data.identityNumber=:identityNumber)");
        if (realCustomer.getId()==-1) {
            query.setParameter("checkId",1);

        }
        else {
            query.setParameter("checkId",0);
        }
        query.setParameter("id", realCustomer.getId());
        if (realCustomer.getName()==null|| realCustomer.getName()=="")
        {
            query.setParameter("checkName",1);
        }
        else query.setParameter("checkName",0);
        query.setParameter("name",realCustomer.getName());
        if (realCustomer.getLastName()==null|| realCustomer.getLastName()=="")
        {
            query.setParameter("checkLastName",1);
        }
        else query.setParameter("checkLastName",0);
        query.setParameter("lastName", realCustomer.getLastName());
        if ( realCustomer.getIdentityNumber()==0)
        {
            query.setParameter("checkIdentityNumber",1);
        }
        else query.setParameter("checkIdentityNumber",0);
        query.setParameter("identityNumber", realCustomer.getIdentityNumber());
        List<RealCustomer> realCustomers=query.getResultList();
        entityManager.close();
        return realCustomers;
    }

    public void update(RealCustomer realCustomer) throws Exception {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        personCustomerDA.update(person);
//        personCustomerDA.close();
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomerUpdate=entityManager.find(RealCustomer.class,realCustomer.getId());
        if (realCustomer.getName()!="")
            realCustomerUpdate.setName(realCustomer.getName());
        if (realCustomer.getLastName()!="")
            realCustomerUpdate.setLastName(realCustomer.getLastName());
        if (realCustomer.getIdentityNumber()!=0)
            realCustomerUpdate.setIdentityNumber(realCustomer.getIdentityNumber());
        if (realCustomer.getNameOfFather()!="")
            realCustomerUpdate.setNameOfFather(realCustomer.getNameOfFather());
        if (realCustomer.getDateOfBirth()!=null)
            realCustomerUpdate.setDateOfBirth(realCustomer.getDateOfBirth());
        entityManager.persist(realCustomerUpdate);
        entityTransaction.commit();
        entityManager.close();

    }
    public static void delete(int id) throws Exception {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        personCustomerDA.delete(id);
//        personCustomerDA.close();
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomer=entityManager.find(RealCustomer.class,id);
        entityManager.remove(realCustomer);
        entityTransaction.commit();
        entityManager.close();
    }
    public List<RealCustomer> findNameFamily(String customerNumber)
    {
        EntityManager entityManager=JPA.getManager();
        System.out.println("in real customer service:"+customerNumber);
        Query query=entityManager.createQuery("select nameFamily from realcustomer nameFamily where nameFamily.customerNumber=:customerNumber");
        query.setParameter("customerNumber",Long.parseLong(customerNumber));
        List<RealCustomer> realCustomers=query.getResultList();
        entityManager.close();
        return realCustomers;
    }
    public RealCustomer findOneById(int id)
    {
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomer=entityManager.find(RealCustomer.class,id);
        entityTransaction.commit();
        entityManager.close();
        return realCustomer;
    }


}

package common.service;

import common.JPA;
import common.Value;
import model.entity.LegalCustomer;
import common.repository.CompanyCustomerDA;
import model.entity.RealCustomer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class LegalCustomerService {
    private  int countCustomers= (int) Value.zero;
    private List<Integer> customerNumbers = new ArrayList<Integer>();
    public void save(LegalCustomer legalCustomer) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        companyCustomerDA.insert(legalCustomer);
//        companyCustomerDA.close();
        countCustomers++;
        for (int i = Value.hundred; i < Value.thousand; i++) {
            customerNumbers.add(new Integer(i));
        }
        Collections.shuffle(customerNumbers);
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        legalCustomer.setCustomerNumber(Long.parseLong(String.valueOf((customerNumbers.get(countCustomers)))));
        entityManager.persist(legalCustomer);
        entityTransaction.commit();
        entityManager.close();
    }

    public Long showCustomerNumber(int id) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        String customerNumber = companyCustomerDA.selectCustomerNumber();
//        companyCustomerDA.close();
//        return customerNumber;
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer=entityManager.find(LegalCustomer.class,id);
        Long customerNumber=legalCustomer.getCustomerNumber();
        entityTransaction.commit();
        entityManager.close();
        return customerNumber;
    }

    public static List<LegalCustomer> findAll() throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        List<LegalCustomer> legalCustomers = companyCustomerDA.select();
//        companyCustomerDA.close();
//        return legalCustomers;
        //FINDALLBYJPAQL
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createQuery("select allData from legalcustomer allData");
        List<LegalCustomer> legalCustomers=query.getResultList();
        entityManager.close();
        return legalCustomers;
    }


    public  List<LegalCustomer> search(LegalCustomer legalCustomer) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        List<LegalCustomer> legalCustomers = companyCustomerDA.search(legalCustomer);
//        companyCustomerDA.close();
//        return legalCustomers;
        EntityManager entityManager=JPA.getManager();
        Query  query=entityManager.createQuery("select data from legalcustomer data where" +
                "(1=:checkId or data.id=:id) AND (1=:checkCompanyName or data.companyName=:companyName)AND(1=:checkEconomicId or data.economicId=:economicId) AND(1=:checkDateOfRegistering or data.dateOfRegistering=:dateOfRegistering)");
        if (legalCustomer.getId()==-1) {
            query.setParameter("checkId",1);

        }
        else {
            query.setParameter("checkId",0);
        }
        query.setParameter("id", legalCustomer.getId());
        if (legalCustomer.getCompanyName()==null|| legalCustomer.getCompanyName()=="")
        {
            query.setParameter("checkCompanyName",1);
        }
        else query.setParameter("checkCompanyName",0);
        query.setParameter("companyName",legalCustomer.getCompanyName());
        if (legalCustomer.getEconomicId()==0)
        {
            query.setParameter("checkEconomicId",1);
        }
        else query.setParameter("checkEconomicId",0);
        query.setParameter("economicId", legalCustomer.getEconomicId());
        if (legalCustomer.getDateOfRegistering()==null)
        {
            query.setParameter("checkDateOfRegistering",1);
        }
        else query.setParameter("checkDateOfRegistering",0);
        query.setParameter("dateOfRegistering", legalCustomer.getDateOfRegistering());
        List<LegalCustomer> legalCustomers=query.getResultList();
        return legalCustomers;
    }
    public void update(LegalCustomer legalCustomer) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        companyCustomerDA.update(legalCustomer);
//        companyCustomerDA.close();
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer1=entityManager.find(LegalCustomer.class,legalCustomer.getId());
        if (legalCustomer.getCompanyName()!="")
            legalCustomer1.setCompanyName(legalCustomer.getCompanyName());
        if (legalCustomer.getEconomicId()!= 0)
            legalCustomer1.setEconomicId(legalCustomer.getEconomicId());
        if (legalCustomer.getDateOfRegistering()!=null)
            legalCustomer1.setDateOfRegistering(legalCustomer.getDateOfRegistering());
        entityManager.persist(legalCustomer1);
        entityTransaction.commit();
        entityManager.close();
    }

    public  void delete(int id) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        companyCustomerDA.delete(id);
//        companyCustomerDA.close();

        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer=entityManager.find(LegalCustomer.class,id);
        entityManager.remove(legalCustomer);
        entityTransaction.commit();
        entityManager.close();
    }
    public LegalCustomer findOneById(int id)
    {
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer=entityManager.find(LegalCustomer.class,id);
        entityTransaction.commit();
        entityManager.close();
        return legalCustomer;
    }
}

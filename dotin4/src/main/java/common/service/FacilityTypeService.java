package common.service;

import common.JPA;
import model.entity.Facilitytype;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Hi on 9/1/2020.
 */
public class FacilityTypeService {
    public void save(Facilitytype facility){
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(facility);
        entityTransaction.commit();
        entityManager.close();
    }
    public List<Facilitytype> findAll()
    {
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createQuery("select allData from facilitytype allData");
        List<Facilitytype> facilities=query.getResultList();
        return facilities;
    }
    public List<Object> findAllNoReapeat()
    {
        EntityManager entityManager=JPA.getManager();
      Query query=entityManager.createNamedQuery("findAllNoReapeat");
        List<Object>  facilities=query.getResultList();
        return facilities;
    }
    public List<Object> findMinMax(String nameOfFacility)
    {
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createNamedQuery("findMinMax");
        query.setParameter("name",nameOfFacility);
        List<Object> objectList=query.getResultList();
        return objectList;
    }

}
